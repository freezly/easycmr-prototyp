import { Component } from '@angular/core';
import {
  NgxQrcodeElementTypes,
  NgxQrcodeErrorCorrectionLevels,
} from '@techiediaries/ngx-qrcode';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page {
  elementType = NgxQrcodeElementTypes.URL;
  correctionLevel = NgxQrcodeErrorCorrectionLevels.HIGH;
  value = 'https://www.techiediaries.com/';
  scannerEnabled = false;
  device;
  scannedText: string;

  constructor() {}

  camerasFoundHandler(event) {
    console.log(event);
    this.device = event[0];
    this.scannerEnabled = true;
  }
  camerasNotFoundHandler(event) {
    console.log(event);
  }
  scanSuccessHandler(event) {
    if (event) {
      console.log('success:', event);
      this.scannedText = event;
    }
  }
  scanErrorHandler(event) {
    console.log(event);
  }
  scanFailureHandler(event) {
    //console.log(event);
  }
  scanCompleteHandler(event) {
    if (event) {
      console.log('complete:', event);
      this.scannerEnabled = false;
    }
  }
}
